import React, { Component } from 'react';

import Aux from '../hoc/Auxiliary';
import PatchnotesComp from '../components/Home/Patchnotes/Patchnotes';
import { vscapeFileRoot } from '../utils/constants';
import axios from '../axios';

export default class Patchnotes extends Component {
  state = {
    patchnotes: '',
  };

  componentDidMount() {
    axios.get(vscapeFileRoot + 'patchnotes.json').then(response => {
      this.setState({ patchnotes: response.data });
    });
  }

  render() {
    return (
      <Aux>
        <PatchnotesComp
          title={'Full Patchnotes'}
          patchnotes={this.state.patchnotes}
          hideFooter
        />
      </Aux>
    );
  }
}
