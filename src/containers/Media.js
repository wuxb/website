import React, { Component } from 'react';

import Aux from '../hoc/Auxiliary';
import Gallery from '../components/UI/Gallery';
import EmbeddedVideo from '../components/UI/EmbeddedVideo';
import { vscapeFileRoot } from '../utils/constants';
import axios from '../axios';

export default class Media extends Component {
  mainpageVideoLink = 'https://www.youtube.com/embed/PYFB65fOtco';

  state = {
    images: [],
  };

  componentDidMount() {
    axios.get(vscapeFileRoot + '/img/gallery/images.json').then(response => {
      const images = Array.from(Array(response.data.imageCount).keys()).map(
        x => {
          return {
            src: `https://vidyascape.org/files/img/gallery/${x}.png`,
          };
        },
      );
      this.setState({ images: images });
    });
  }

  render() {
    return (
      <Aux>
        <EmbeddedVideo title="Intro video" videoSrc={this.mainpageVideoLink} />
        <Gallery images={this.state.images} />
      </Aux>
    );
  }
}
